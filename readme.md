

# DEMO 说明V1

## 1. 运行DEMO

### 1.1 启动

- 开发环境编译代码： `npm run watch`
- 运行： `npm run start`
- 查看标准输出： `pm2 log`
- （简单的）生产环境运行 `npm run start:prod`

### 1.2 效果
-  访问localhost:2333/
    1. 未登录则进入登录页
    2. 填写信息登陆后进入主页
    3. 点击登出则回到登录页


## 2. 代码说明
###  2.1 目录结构


- 每个文件中包含的内容目前还相对简单，可以直接获取源码查看详细注释和代码。

```
koa-rubick
├── package.json
├── webpack.config.js ()
└── src
    ├── index.js
    ├── env.js
    ├── common
    |   ├── error_handler.js (通用错误处理)
    |   ├── exceptions.js (通用异常类)
    |   ├── middlewares.js (通用中间件)
    |   ├── request.js (公共请求)
    |   ├── model.js （公共model, User，RestModel等，包括1.对部分资源的request封装 2.User用于请求）
    |   └── weblab_client.js (从lightkeepr获取weblab状态)
    ├── config
    |   ├── config.js
    |   └── logger.js
    ├── views ！！【业务代码将主要在这里】！！
    |   ├── application.js (application相关请求转发)
    |   ├── console.js
    |   ├── landing.js
    |   └── region.js
    ├── models  ！！【业务代码将主要在这里】！！
    |   ├── service.js (公共model--namepaceModel的实例)
    |   └── ... 可选（后续用于继承自公共model的其他model，用于views中的中间件调用）
    ├── templates (underscore模板)
    |   ├── index.html
    |   └── index-landing.html
    ├── routes
    |   └── routes.js
    └── public
        └── static (增加一层static仅仅为了兼容当前angularjs中代码)
            ├── assets (django下面的assets文件夹)
            ├── fonts (django下面的fonts文件夹)
            └── images (django下面的images文件夹)


```
_*清单2.1 目录结构*_

### 2.2 代码文件说明 （按照上述清单中目录结构从上至下描述）
- index.js
    - 入口文件，各个中间件的调用
- env.js
    - 应改为int.env, 由插件读取，尚未添加
- error_handler.js
    - 错误捕捉中间件，当所有中间件都执行完毕后仍然没得到响应，最终抛出一个异常。
    - 自定义ajax错误处理，即将错误以json格式响应并设置相应的状态码。
    - **TODO**还需细化
- exceptions.js
    - 通用异常类，继承自Node.js，按照atropos规范生成必要的字段 https://bitbucket.org/mathildetech/atropos/wiki/projects/common/error-response-specification
- middleware.js
    - login_required 类似django中的login_required中间件，若请求到达时发现没通过authentication则跳转至登录页。
    - authentication 类似django中的authentication，把用户信息保存至http请求对象中
- request.js
    - Request  使用node.js库request作为请求客户端，由于默认是回调函数版本，本处为应用于koa2则使用request-promise版本
    - JakiroRequest 对Request的封装，统一处理发送至JakiroV1的请求
    - **TODO**JakiroV2等
- model.js
    - RESTModel 类似django 的 RESTModel，旨在封装统一的list，update，create等请求
    - User 用于验证会话
- weblab_client.js
    - 目前仅实现了向lightkeeper发送请求部分
    - **TODO**还需添加本地mock环境变量读取部分
- config.js
    - 各个第三方库中间件所需要的配置
        - 路由
        - 错误捕捉， 包括错误页面的输出可以从这里渲染（比较统一）**TODO**加入403页面等
        - session
        - 静态文件目录 可以实现gzip等
        - view
        - csrf **TODO** 修改前端csrf头与koa插件匹配
        - ...  **TODO** redis、验证码等等配置、PM2配置等
    - logger
        - 日志轮转模块
        - **TODO**还需实现类似django中的logger.debug和logger.info并能够输出至轮转日志文件中
        - **TODO**也可以考虑用pm2-logrotate来实现
- views/*
    - 业务代码，目前包括：
        - 登录页面view
        - 登录部分ajax请求
        - 控制台页面view
        - 控制台首页部分ajax请求
    - **TODO**还需包括
        - 其他业务
- models/*
    - 业务代码，
       - 目前包括：service列表获取
       - **TODO**还需包括其他app,repo等等
- templates
    - 首页underscore模板
    - 登录模板
- routes/*
    - 定义views ,ajax 路由，和我们的django项目相仿很好理解
    - **TODO**还需要ping，django url，pattern里其他的。。
- public
    - 目前将django下面的static文件夹部分内容（已经构建好的bundles）搬了过来
    - **TODO** 修改前端webpack配置使资源构建至该目录下



## **TODO**
- docker-pm2
- docker-compose

