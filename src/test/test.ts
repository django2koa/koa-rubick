import { expect } from 'chai';
import * as _ from 'lodash';
import testApp from '../index';
import * as request from 'supertest';
import { describe, it } from 'mocha';

const app = request(testApp);

describe("test ajax api", function () {
    it('should be get 404 response', done => {
        app.post('/landing/login/requires-captcha/')
            .expect(404)
            .end(done);
    });
});