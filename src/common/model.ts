import * as _ from 'lodash';
import { JakiroRequest } from './request';

class RESTModel {
  endpoint: string
  constructor(url) {
    this.endpoint = url;
  }

  get_url(ctx?) {
    return this.endpoint;
  }

  list(ctx, paginate = false) {
    return JakiroRequest(ctx, 'GET', this.get_url(ctx), ctx).then(body => {
      if (body) {
        return _.get(body, 'results')
      } else {
        return [];
      }
    });

  }
}

class NamespaceModel extends RESTModel {
  constructor(url) {
    super(url);
  }

  get_url(ctx) {
    return this.endpoint + '/' + _.get(ctx.query, 'namespace');
  }
}

class User {
  username: String;
  token: String;
  namespace: String;
  constructor(username, namepace, token) {
    this.username = username;
    this.username = namepace;
    this.token = token;
  }

  get isAuthenticated() {
    return this.username && this.token;
  }

}

export { RESTModel, NamespaceModel, User };