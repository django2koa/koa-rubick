let request=require('request-promise'); // dont know why es6 module cant work temporary
import * as _ from 'lodash';
import ENV_VARS from '../env';
import rbLogger from 'logger/logger';

let logger = rbLogger.logger;

let debug = require('request-debug');
// request.debug = true;
// debug(request);

export function Request(option) {
  return request(option).then(function (res) {
  logger.debug(`REQUEST: ${option.method} ${option.url} headers: ${JSON.stringify(option.headers)} 200: ${JSON.stringify(res)}`);
    return res;
  }).catch(function (err) {
    logger.error(`REQUEST ${option.method} ${option.url} headers: ${JSON.stringify(option.headers)} Error: ${err.statusCode}`);
    throw err;
  });
}

export function JakiroRequest(ctx:any, method, path, args?) {

  let option = {
    url: ENV_VARS['API_SERVER_URL'] + 'v1' + path,
    json: true,
    headers: {
      'User-Agent': 'rubick/v1.0'
    },
    method,
    qs: _.get(args, 'query'),
    useQuerystring: true
  };

  if (_.get(ctx, 'user.token')) {
    option.headers['Authorization'] = `Token ${_.get(ctx.session, 'user.token')}`;
  }

  if (_.get(args, 'data')) {
    option['body'] = args.data;
  }

  return Request(option);
}
