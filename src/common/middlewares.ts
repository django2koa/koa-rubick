/**
 * Created by wsk on 17/4/26.
 */
import * as _ from 'lodash';
import { User } from './model';

const LOGIN_URL = '/landing/login/';

function _redirectToLogin(ctx) {
  let loginUrl = LOGIN_URL;
  if (!_isLoginUrl(ctx.request.href)) {
    loginUrl += `?next=${ctx.request.href}`;
  }
  ctx.redirect(loginUrl);
  ctx.status = 302;
}

function _isLoginUrl(url = '') {
  return !!url.includes(LOGIN_URL);
}

function login_required(ctx, next) {
  let is_ajax = ctx.request.get('X-Requested-With') === 'XMLHttpRequest';

  if (is_ajax) {
    if (!ctx.user || !ctx.user.isAuthenticated) {
      ctx.throw(401);
    } else {
      return next();
    }
  } else {
    if (!ctx.user || !ctx.user.isAuthenticated) {
      _redirectToLogin(ctx);
    }
  }
  return next();
}

function authentication(ctx, next) {
  let user:any = _.get(ctx, 'session.user');
  if (user) {
    ctx.user = new User(user.username, user.namespace, user.token);
  }
  return next();
}

export { authentication, login_required }