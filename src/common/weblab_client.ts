import ENV_VARS from '../env';
import { JakiroRequest, Request } from '../common/request'
import { RubickErrorResponse } from '../common/exceptions'

class WeblabClient {
  url: any;
  headers: any;

  constructor() {
    this.url = ENV_VARS['LIGHTKEEPER_ENDPOINT'] + 'weblab-states/';
    this.headers = {
      'Accept': 'application/json',
      'User-Agent': 'AlaudaClient/'
    };
    // this.max_timeout = 2;
  }

      
  prefetch(namespace, weblabs = []) {
    if (ENV_VARS['LIGHTKEEPER_OVERRIDDEN_WEBLABS']) {
      return this.parseOverriddenWeblabs(ENV_VARS['LIGHTKEEPER_OVERRIDDEN_WEBLABS']);
    }
    let url = this.url;
    if (namespace) {
      url += namespace + '/';
    }
    if (weblabs.length) {
      url += '?weblabs=' + weblabs.join(',');
    }
    return Request({
      url,
      method: 'GET',
      headers: this.headers,
      // timeout: this.max_timeout,
      qs: {
        weblabs
      },
      json: true
    }).then(res => res)
      .catch(err => {
        console.error('Failed to retrieve weblab states. ', typeof err, err.message)
      });
  }

  parseOverriddenWeblabs(str) {
    let weblabStrArr = str.split(',');
    let weblabs = {};
    weblabStrArr.forEach((str) => {
      let weblab = str.split(':');
        weblabs[weblab[0].toLowerCase()] = weblab[1] == 1 ? true : false
    });
    console.log(weblabs)
    return weblabs;
  }
}

const weblabClient = new WeblabClient();
export default weblabClient;