import * as log4js from 'koa-log4';
import * as config from '../config/config';
import * as path from 'path';
import * as _ from 'lodash';

// must use the format of 'var = require' when use typescript to load json file
var defaultDevLogConfig = require('./default_conf/log_dev.json');
var defaultProdLogConfig = require('./default_conf/log_prod.json');

class RubickLogger {
  customConfig: any;
  public logger: any;
  constructor(config = {}) {
    this.customConfig = config;
    log4js.configure(this.getEnvConfig());
    this.logger = log4js.getLogger('log');
  }
  public getHttpLoggerMiddleware() {
    return log4js.koaLogger(log4js.getLogger("access"), { level: 'info' });
  }
  private getEnvConfig() {
    let envConfig;
    let setAppenderPath = (appender) => {
      if (appender.type === 'logLevelFilter') {
        appender.appender.filename = path.join(this.customConfig.logDir, appender.appender.filename);
      }
    }
    if (process.env.NODE_ENV !== 'production') {
      envConfig = _.cloneDeep(defaultDevLogConfig);
    } else {
      envConfig = _.cloneDeep(defaultProdLogConfig);
    }
    envConfig.appenders.forEach(setAppenderPath);
    return envConfig;
  }
}
let rbLogger = new RubickLogger(config.default.loggerConfig);

export default rbLogger;