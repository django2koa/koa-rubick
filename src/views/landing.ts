// import authenticate from '../common/auth'
import { JakiroRequest } from '../common/request';

async function landingView(ctx) {
  await ctx.render('index-landing.html');
}

const LOGIN_ATTEMPTS_THRESHOLD = 3;

async function loginView(ctx) {
  ctx.session['LOGIN_ATTEMPTS_COOKIE_NAME'] = (ctx.session['LOGIN_ATTEMPTS_COOKIE_NAME'] || 0) + 1;

  if (ctx.session['LOGIN_ATTEMPTS_COOKIE_NAME'] > LOGIN_ATTEMPTS_THRESHOLD) {

  }
  let data = ctx.request.body;

  if (data.email) {
    data = { 'email': data.email, 'password': data.password }
  } else {
    data.username = data.account;
    delete data.account;
  }
  // try {
  const result = await JakiroRequest(ctx, "POST", "/generate-api-token/", { data });
  if (result.token) {
    ctx.session.user = result;
    ctx.body = { url: '/console/' };
  }
  // } catch (res) {
  //   这部分注释掉的错误会被顶层错误处理捕捉到
  //   ctx.body = res.error;
  //   ctx.status = res.statusCode;
  // }
}

async function logoutView(ctx) {
  this.session = null;
  ctx.redirect('/landing/login/');
}

async function consoleRedierctView(ctx) {
  await ctx.render('index.html');
}
export { landingView, loginView, logoutView, consoleRedierctView };