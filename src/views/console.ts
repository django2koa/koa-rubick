import * as _ from 'lodash';
import { JakiroRequest } from '../common/request';
import weblabClient from '../common/weblab_client';

async function consoleView(ctx) {
  let namespace = _.get(ctx.session, 'user.namespace');
  let username = _.get(ctx.session, 'user.username');
  await ctx.render('index.html', { namespace, username });
}

async function featureView(ctx) {
  const feature_flags = await weblabClient.prefetch(_.get(ctx.query, 'namespace'));
  ctx.body = { feature_flags: feature_flags };

}

//----------------account related-------
async function profileView(ctx) {
  const profile = await JakiroRequest(ctx, 'GET', `/auth/${_.get(ctx.query, 'namespace')}/profile/`);
  ctx.body = profile;
}

async function OrgMemberView(ctx) {
  const result = await JakiroRequest(ctx,
    'GET', `/orgs/${_.get(ctx.query, 'org_name') || _.get(ctx.query, 'namespace')}/accounts/${_.get(ctx.query, 'username')}`);
  ctx.body = result;
}

export { consoleView, featureView, profileView, OrgMemberView };