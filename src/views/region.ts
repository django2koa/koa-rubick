import { JakiroRequest } from '../common/request';
import * as _ from 'lodash';

async function RegionListView(ctx) {
  const result = await JakiroRequest(ctx, 'GET', `/regions/${_.get(ctx.query, 'namespace')}/`);
  ctx.body = { regions: result };
}

export { RegionListView };