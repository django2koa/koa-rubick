import  Service  from '../models/service';
import * as _ from 'lodash';

async function ApplicationListView(ctx) {
  let param = ctx.query || {};
  const service_list = await get_service_list(ctx, param);

  ctx.body = { 'list': service_list }
}

async function get_service_list(ctx, params) {
  let service_list = await Service.list(ctx, ctx);

  let result_list = [];
  let pick = ['uuid', 'current_status', 'namespace', 'service_name',
    'image_name', 'image_tag', 'instance_size', 'custom_instance_size',
    'volumes', 'scaling_mode', 'application',
    'autoscaling_config', 'target_num_instances',
    'tasks_healthy', 'health_checks', 'health_status', 'instances',
    'health_check_results', 'privilege', 'healthy_num_instances', 'healthy',
    'is_stateful', 'created_at', 'resource_actions'];

  _.forEach(service_list, (service) => {

    if (!service['health_status']) {
      if (service['scaling_mode'] !== 'AUTO') {
        if (service['healthy_num_instances'] < service['target_num_instances']) {
          service['health_status'] = 'unhealthy';
        }
        else {
          service['health_status'] = 'healthy';

        }
      }
    }
    let item = _.pick(service, pick);

    result_list.push(item)
  });
  return result_list;

}

export { ApplicationListView };