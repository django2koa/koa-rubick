import { jsonErrorHandler } from'../common/error_handler'
const dirname = process.cwd() + '/dist/';
const MATHILDE_COMPONENT = 'rubick';
const config = {
  secretKeys: ['some secrhurr'],
  sessionConf: {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    maxAge: 86400000, /** (number) maxAge in ms (default is 1 days) */
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
  },
  staticConf: dirname + '/public',
  viewsConf: {
    root: dirname + 'templates',
    opts: {
      map: {
        html: 'underscore'
      }
    }
  },
  loggerConfig: {
    logDir: '/var/log/mathilde/' + MATHILDE_COMPONENT
  },
  errorConfig: {
    json: jsonErrorHandler,
    accepts: function () {
      if (this.accepts('html')) {
        return 'html';
      } else {
        return 'json';
      }
    }
  },
  csrfConfig: {
    invalidSessionSecretMessage: 'Invalid session secret',
    invalidSessionSecretStatusCode: 403,
    invalidTokenMessage: 'Invalid CSRF token',
    invalidTokenStatusCode: 403,
    excludedMethods: ['GET', 'HEAD', 'OPTIONS'],
    disableQuery: false
  }
};

export default config;