import * as Router from 'koa-router';
import { consoleView, featureView, profileView, OrgMemberView } from '../views/console';
import { landingView, loginView, logoutView } from '../views/landing';
import { RegionListView } from '../views/region';
import { ApplicationListView } from '../views/application';
import { login_required } from '../common/middlewares';

const viewRouter = Router();
const ajaxRouter = Router({ prefix: '/ajax' });

viewRouter.get(/^\/console(?:\/|$)/, login_required, consoleView);
viewRouter.get('/', login_required, consoleView);
viewRouter.get('/landing/login', landingView);
viewRouter.get('/ap/logout', logoutView.bind(this));

ajaxRouter.post('/landing/login/', loginView.bind(this));
ajaxRouter.get('/landing/login/requires-captcha/', (ctx) => {
  ctx.body = { result: false }
});
ajaxRouter.get('/captcha-refresh/', (ctx) => {
  //image_url: "/captcha-image/6c0888033320ca05734575905958edd077d727d9/"
});
ajaxRouter.get('/account/feature-flags', login_required, featureView.bind(this));
ajaxRouter.get('/account/profile', login_required, profileView.bind(this));
ajaxRouter.get('/permission/org/member', login_required, OrgMemberView.bind(this));
ajaxRouter.get('/region/list', login_required, RegionListView.bind(this));
ajaxRouter.get('/application/list', login_required, ApplicationListView.bind(this));

export { viewRouter, ajaxRouter };