import * as Koa from 'koa';
import * as koaBodyParser from 'koa-bodyparser';
import * as koaMorgan from 'koa-morgan';
import * as koaViews from 'koa-views';
import * as koaStatic from 'koa-static';
import * as koaSession from 'koa-session';
import * as koaOnError from 'koa-onerror';
import * as koaQs from 'koa-qs';
import * as CSRF from 'koa-csrf';

import { authentication } from './common/middlewares';
import config from './config/config'
import { commonErrorHandlerMiddleware } from './common/error_handler';
import { viewRouter, ajaxRouter } from './routes/routes';
import rbLogger from './logger/logger'
// init Koa
const app = new Koa();

// config Koa
koaOnError(app, config.errorConfig); // error
koaQs(app); // query string
app.keys = config.secretKeys;

// 配置中间件
// let logger = koaMorgan(config.logConf.format);
let httpLoggerMiddleware = rbLogger.getHttpLoggerMiddleware();
let session = koaSession(config.sessionConf, app);
let csrfMiddleware = new CSRF(config.csrfConfig);
let staticServer = koaStatic(config.staticConf);
let views = koaViews(config.viewsConf.root, config.viewsConf.opts);
let parser = koaBodyParser();

// 使用中间件
app
.use(httpLoggerMiddleware)
  .use(commonErrorHandlerMiddleware)  //如果直到最后一个中间件都处理完时，依然没有响应，则默认产生404错误
  .use(staticServer) // 静态服务器，服务public目录下文件包括html,css,js
  .use(views) //视图渲染
  .use(session) // 给每次创建的request对象增加一个session属性从而可以方便的访问
  .use(authentication) // 为请求上下文增加user对象，方便的获取用户信息验证会话有效性
  .use(parser) // 解析http请求body内容
  // .use(csrfMiddleware)   // Todo: 由于当前angularjs中csrf header名字设置与插件中不符合，启用一定会报错，还需要修改csrf在http内的头字段
  .use(viewRouter.routes()) // 请求页面路由
  .use(ajaxRouter.routes()) // ajax请求路由
  .use(ajaxRouter.allowedMethods({
    throw: true
  }));

//run Koa
const testApp = app.listen(2333, () => {
  rbLogger.logger.info('Sever is start, Listening on port 2333');
});

export default testApp;
